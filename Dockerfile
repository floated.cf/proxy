FROM tutum/nginx

RUN rm -v /etc/nginx/sites-enabled/default
ADD sites /etc/nginx/sites-enabled/

EXPOSE 80
